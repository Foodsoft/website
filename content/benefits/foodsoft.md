---
title: "Foodsoft?"
icon: "fa-apple-alt"
weight: 1
---

Foodsoft ist ein Tool für [Lebensmittelkooperativen](https://de.wikipedia.org/wiki/Lebensmittelkooperative), welches selbstorganisierte gemeinsame Bestellungen in Großmengen von regionalen und ökologischen Produkten vereinfacht und transparent gestaltet.

Foodsoft wird  entwickelt und betrieben von [foodcoops.net](https://foodcoops.net/)
