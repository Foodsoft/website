---
title: "Unser Vorhaben"
icon: "fa-cogs"
weight: 3
---

* ✅  Technische Schuld reduzieren
* ✅  Ruby on Rails Upgrade
* ✅  Artikel Import verbessern
      (Großhandelschnitstelle)
* ✅ Userexperience Verbessern

[Mehr dazu hier](https://git.local-it.org/Foodsoft/foodsoft/src/branch/demo/README.md)