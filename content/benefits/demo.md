---
title: Demo
icon: "fa-rocket"
weight: 4
---

Probier die Foodsoft doch einfach mal auf unserer [Demo-Instanz](https://foodsoft.dev.local-it.cloud) aus!

Administratorin: `alice`  
User: `bob`  
Passwort: `secret`    